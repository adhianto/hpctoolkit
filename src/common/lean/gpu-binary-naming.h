// SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef gpu_binary_naming_h
#define gpu_binary_naming_h

//*****************************************************************************
// macros
//*****************************************************************************

#define GPU_BINARY_NAME           "gpubin"

#define GPU_BINARY_SUFFIX         "." GPU_BINARY_NAME
#define GPU_BINARY_DIRECTORY      GPU_BINARY_NAME "s"



#endif
