// SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef FileNameMap_hpp
#define FileNameMap_hpp

std::string &getRealPath(const char *name);

#endif
