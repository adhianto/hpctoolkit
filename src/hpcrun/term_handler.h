// SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef TERM_HANDLER_H
#define TERM_HANDLER_H

int
hpcrun_setup_term();

#endif
