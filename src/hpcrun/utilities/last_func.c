// SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

/* provided to figure out where libcsprof's .text section ends */
void
hpcrun_last_func()
{
}
