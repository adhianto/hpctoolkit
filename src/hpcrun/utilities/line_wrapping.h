// SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef __LINE_WRAPPING_H__
#define __LINE_WRAPPING_H__

int strwrap(char * s, int w, char *** line_ret, int ** len_ret);

#endif
