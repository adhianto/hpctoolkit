// SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#define _GNU_SOURCE

#include "validate_return_addr.h"

void
hpcrun_validation_summary(void)
{
  ; // do nothing by default
}
