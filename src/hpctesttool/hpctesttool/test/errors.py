# SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: BSD-3-Clause


class PredictableFailureError(Exception):
    pass
