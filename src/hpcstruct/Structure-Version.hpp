// SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*-

#ifndef Structure_Version_hpp
#define Structure_Version_hpp

//***************************************************************************
// interface operations
//***************************************************************************

bool StructureFileCheckVersion(const char *structureFileName);



#endif // Structure_Version_hpp
